package com.sogeti.algo17RPG;
import com.sogeti.algo17RPG.model.*;
import java.util.ArrayList;

public class Personnage {
	private int id;
	private String nom;
	private int pv;
	private int pointAction;
	private int niveau;
	private Arme armePorter;
	private Armure armurePorter;
	private ArrayList<Butin> leButin;
	private Classe classe;
	
	
	/**
	 * @param id
	 * @param nom
	 * @param pv
	 * @param pointAction
	 * @param niveau
	 * @param armePorter
	 * @param armurePorter
	 * @param leButin
	 * @param classe
	 */
	public Personnage(int id, String nom, int pv, int pointAction, int niveau, Arme armePorter, Armure armurePorter,
			ArrayList<Butin> leButin, Classe classe) {
		super();
		this.id = id;
		this.nom = nom;
		this.pv = pv;
		this.pointAction = pointAction;
		this.niveau = niveau;
		this.armePorter = armePorter;
		this.armurePorter = armurePorter;
		this.leButin = leButin;
		this.classe = classe;
	}
	/**
	 * @param id
	 * @param nom
	 * @param pv
	 * @param pointAction
	 * @param niveau
	 * @param armePorter
	 * @param armurePorter
	 
	 * @param classe
	 */
	public Personnage(int id, String nom, int pv, int pointAction, int niveau, Arme armePorter, Armure armurePorter,
			Classe classe) {
		super();
		this.id = id;
		this.nom = nom;
		this.pv = pv;
		this.pointAction = pointAction;
		this.niveau = niveau;
		//if (!prendEnMain(armePorter)) {
		//	this.armePorter = null;
		//	System.out.println("mais je n'ai pas d'arme");
		//}
		//else {
			this.armePorter = armePorter;
		//}
		
		//if (!porter(armurePorter)) {
		//	this.armePorter = null;
		//	System.out.println("mais je n'ai pas d'armure");
		//}
		//else {
			this.armurePorter = armurePorter;
		//}
		
		
		this.leButin = new ArrayList<Butin>();
		this.classe = classe;
	}
	public boolean checkArme(Arme arme) {
		return false;
	}
	public ArrayList<Butin> getLeButin() {
		return leButin;
	}

	public void setLeButin(ArrayList<Butin> leButin) {
		this.leButin = leButin;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getPv() {
		return pv;
	}
	public void setPv(int pv) {
		this.pv = pv;
	}
	public int getPointAction() {
		return pointAction;
	}
	public void setPointAction(int pointAction) {
		this.pointAction = pointAction;
	}
	public int getNiveau() {
		return niveau;
	}
	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}
	public Arme getArmePorter() {
		return armePorter;
	}
	public void setArmePorter(Arme armePorter) {
		this.armePorter = armePorter;
	}
	public Armure getArmurePorter() {
		return armurePorter;
	}
	public void setArmurePorter(Armure armurePorter) {
		this.armurePorter = armurePorter;
	}

	public Classe getClasse() {
		return classe;
	}
	public void setClasse(Classe classe) {
		this.classe = classe;
	}
	/*
	 * attaquer
	 * 
	 * appele la fonction de la Classe du personnage pour attaquer
	 * 
	 * 
	 * */
	public int attaquer() {
		int action = this.pointAction;
		int dega = 0;
		while(action > this.armePorter.getPointAction()) {
			action -= this.armePorter.getPointAction();
			dega = this.classe.attaquer(armePorter);
		}
		return dega;
	}
	/*
	 * defendre
	 * 
	 * appele la fonction de la Classe du personnage pour attaquer
	 * 
	 * 
	 * */
	public int defendre() {
		return this.classe.defendre(armurePorter);
	}
	/*
	 * porter
	 * 
	 * appele la fonction de la Classe du personnage pour pouvoir porter une armure
	 * 
	 * 
	 * */
	public boolean porter() {
		
		return this.classe.porter(this.armurePorter);
		
	}
	public boolean porter(Armure armureDonner) {
		
		return this.classe.porter(armureDonner);
		
	}
	/*
	 * prendEnMain
	 * 
	 * appele la fonction de la Classe du personnage pour pouvoir porter une arme
	 * 
	 * 
	 * */
	public boolean prendEnMain() {
		return this.classe.prendEnMain(this.armePorter);
	}
	public boolean prendEnMain(Arme armeDonner) {
		return this.classe.prendEnMain(armeDonner);
	}
	
	

}
