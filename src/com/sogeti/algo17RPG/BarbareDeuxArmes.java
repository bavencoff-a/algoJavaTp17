package com.sogeti.algo17RPG;
import com.sogeti.algo17RPG.model.*;
import java.util.Random;

public class BarbareDeuxArmes implements Barbare {
	
	private int[] peutEquiper;
	private Random rand = new Random();
	private Arme deuxiemArme = new Arme(TypeArme.Physique,1,2,"poing");
	
	public BarbareDeuxArmes(int[] peutEquiper) {
		super();
		this.peutEquiper = peutEquiper;
	}


	public int[] getPeutEquiper() {
		return peutEquiper;
	}

	public void setPeutEquiper(int[] peutEquiper) {
		this.peutEquiper = peutEquiper;
	}
	/*
	 * attaqueDouble
	 * 
	 * attaque avec ses deux armes
	 * 
	 * 
	 * */
	public int attaqueDouble() {
		int degasup = 0;
		if(rand.nextInt(4 - 1) == 2) {
			degasup = 3;
			System.out.println("attaque double !!");
		}
		return degasup;
	}
	/*
	 * attaquer
	 * 
	 * attaque l'attaque durant le tour
	 * 
	 * 
	 * */
	@Override
	public int attaquer(Arme epee) {
		// TODO Auto-generated method stub
		int valeurDega = 0;
		valeurDega = attaqueDeuxiemMain(epee) + epee.getPointDeDegat() + attaqueDouble();
		
		
		return valeurDega;
	}
	/*
	 * porter
	 * 
	 * permet de savoir si le barbare peut porter l'armure mis en parametre
	 * 
	 * 
	 * */
	@Override
	public boolean porter(Armure mail) {
		boolean peutPorterCa = false;
		for (int i : peutEquiper) {
			if (mail.getTypeArmure().ordinal() == i) {
				peutPorterCa = true;
			}
		}
		
		return peutPorterCa;
	}
	/*
	 * prendEnMain
	 * 
	 * permet de savoir si le barbare peut porter l'arme mis en parametre
	 * 
	 * 
	 * */
	@Override
	public boolean prendEnMain(Arme epee) {
		boolean peutPorterCa = false;
		for (int i : peutEquiper) {
			if (epee.getTypeArme().ordinal() == i) {
				peutPorterCa = true;
			}
		}
		
		return peutPorterCa;
	}
	/*
	 * attaqueDeuxiemMain
	 * 
	 * attaque avec son arme secondaire
	 * 
	 * 
	 * */
	@Override
	public int attaqueDeuxiemMain(Arme epeeDeux) {
		// TODO Auto-generated method stub
		return epeeDeux.getPointDeDegat()-1;
	}

	@Override
	public int defendre(Armure armure) {
		// TODO Auto-generated method stub
		
		return armure.getPointDeProtection();
	}


	@Override
	public boolean equipeDeuxiemArme(Arme arme) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public Arme getDeuxiemArme() {
		// TODO Auto-generated method stub
		return this.deuxiemArme;
	}


	@Override
	public boolean setDeuxiemArme(Arme arme) {
		// TODO Auto-generated method stub
		boolean trouve = false;
		if (prendEnMain(arme)) {
			this.deuxiemArme = arme;
			trouve = true;
		}
		return trouve;
	}
	
	
}
