package com.sogeti.algo17RPG;
import java.util.Random;
import com.sogeti.algo17RPG.model.*;
public class Croiser implements Paladin {
	//+attaquerdeuxMain(): int
	//+porter(Armure): bool
	//+prendEnMain(Arme): bool
	private int[] peutEquiper;
	private Random rand = new Random();

	public Croiser(int[] peutEquiper) {
		super();
		this.peutEquiper = peutEquiper;
	}

	public int[] getPeutEquiper() {
		return peutEquiper;
	}

	public void setPeutEquiper(int[] peutEquiper) {
		this.peutEquiper = peutEquiper;
	}
	/*
	 * attaquer
	 * 
	 * attaque avec son arme principale
	 * applique un bonus de degat car le crois�e la manie a deux mains
	 * 
	 * 
	 * */
	@Override
	public int attaquer(Arme epee) {
		// TODO Auto-generated method stub
		Double dega = epee.getPointDeDegat() * 1.5;
		return dega.intValue();
	}

	@Override
	/*
	 * porter
	 * 
	 * permet de savoir si le barbare peut porter l'armure mis en parametre
	 * 
	 * 
	 * */
	public boolean porter(Armure mail) {
		boolean peutPorterCa = false;
		for (int i : peutEquiper) {
			if (mail.getTypeArmure().ordinal() == i) {
				peutPorterCa = true;
			}
		}
		
		return peutPorterCa;
	}
	/*
	 * prendEnMain
	 * 
	 * permet de savoir si le barbare peut porter l'arme mis en parametre
	 * 
	 * 
	 * */
	@Override
	public boolean prendEnMain(Arme epee) {
		boolean peutPorterCa = false;
		for (int i : peutEquiper) {
			if (epee.getTypeArme().ordinal() == i) {
				peutPorterCa = true;
			}
		}
		
		return peutPorterCa;
	}

	@Override
	public int soin() {
		// TODO Auto-generated method stub
		System.out.println("GLORYA !!");
		return rand.nextInt(6 - 1);
	}

	@Override
	public int defendre(Armure armure) {
		// TODO Auto-generated method stub
		return armure.getPointDeProtection();
	}
	

}
