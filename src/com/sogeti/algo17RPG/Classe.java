package com.sogeti.algo17RPG;
import com.sogeti.algo17RPG.model.*;

public interface Classe {

	//+attaquer(Arme): int
	//+porter(Armure): bool
	//+prendEnMain(Arme): bool
	public abstract int attaquer(Arme epee);
	public abstract boolean porter(Armure mail);
	public abstract boolean prendEnMain(Arme epee);
	public abstract int defendre(Armure armure);
}
