package com.sogeti.algo17RPG;
import com.sogeti.algo17RPG.model.*;

public interface Barbare extends Classe {

	//+attaqueDeuxiemMain(Arme): int
	public abstract int attaqueDeuxiemMain(Arme epeeDeux);
	
	public abstract boolean equipeDeuxiemArme(Arme arme);
	public abstract Arme getDeuxiemArme();
	public abstract boolean setDeuxiemArme(Arme arme);
}
