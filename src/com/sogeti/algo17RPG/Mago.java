package com.sogeti.algo17RPG;
import com.sogeti.algo17RPG.model.*;
public class Mago implements Magicien {
	private int[] peutEquiper;
	private int mana;

	public Mago(int[] peutEquiper) {
		super();
		this.peutEquiper = peutEquiper;
		this.mana = 3;
	}
	public void initMana() {
		this.mana = 3;
	}

	public int[] getPeutEquiper() {
		return peutEquiper;
	}

	public void setPeutEquiper(int[] peutEquiper) {
		this.peutEquiper = peutEquiper;
	}

	@Override
	public int attaquer(Arme epee) {
		// TODO Auto-generated method stub
		return epee.getPointDeDegat();
	}
	/*
	 * porter
	 * 
	 * permet de savoir si le barbare peut porter l'armure mis en parametre
	 * 
	 * 
	 * */
	@Override
	public boolean porter(Armure mail) {
		boolean peutPorterCa = false;
		for (int i : peutEquiper) {
			if (mail.getTypeArmure().ordinal() == i) {
				peutPorterCa = true;
			}
		}
		
		return peutPorterCa;
	}
	/*
	 * prendEnMain
	 * 
	 * permet de savoir si le barbare peut porter l'arme mis en parametre
	 * 
	 * 
	 * */
	@Override
	public boolean prendEnMain(Arme epee) {
		boolean peutPorterCa = false;
		for (int i : peutEquiper) {
			if (epee.getTypeArme().ordinal() == i) {
				peutPorterCa = true;
			}
		}
		
		return peutPorterCa;
	}

	@Override
	public int sort() {
		// TODO Auto-generated method stub
		int protec = 0;
		if(mana > 0) {
			protec = 3;
			mana --;
			System.out.println("Sort de Protection !!");
		}
		return protec;
		
	}

	@Override
	public int defendre(Armure armure) {
		// TODO Auto-generated method stub
		return armure.getPointDeProtection() + sort();
	}
	
}
