package com.sogeti.algo17RPG;
import com.sogeti.algo17RPG.model.*;

import java.util.*;

public class Hero extends Personnage {
	private int pointDestin;
	private int experience;
	/**
	 * @param id
	 * @param nom
	 * @param pv
	 * @param pointAction
	 * @param niveau
	 * @param armePorter
	 * @param armurePorter
	 * @param leButin
	 * @param classe
	 * @param pointDestin
	 * @param experience
	 */
	public Hero(int id, String nom, int pv, int pointAction, int niveau, Arme armePorter, Armure armurePorter,
			ArrayList<Butin> leButin, Classe classe, int pointDestin, int experience) {
		super(id, nom, pv, pointAction, niveau, armePorter, armurePorter, leButin, classe);
		this.pointDestin = pointDestin;
		this.experience = experience;
	}
	
	/**
	 * @param id
	 * @param nom
	 * @param pv
	 * @param pointAction
	 * @param niveau
	 * @param armePorter
	 * @param armurePorter
	 * @param classe
	 * @param pointDestin
	 * @param experience
	 */
	public Hero(int id, String nom, int pv, int pointAction, int niveau, Arme armePorter, Armure armurePorter
			, Classe classe, int pointDestin, int experience) {
		super(id, nom, pv, pointAction, niveau, armePorter, armurePorter, classe);
		this.pointDestin = pointDestin;
		this.experience = experience;
	}
	
	/*
	 * monterNiveaux:
	 * permet de savoir si le hero doit monter de niveaux ou non en fonction de son experience total
	 * s'execute e la fin de chaque combat remporter
	 * PARAM :xp est l'experience total du personnage
	 * Return true si il monte de niveaux
	 * */
	private boolean monterNiveaux(int xp) {
		return false;
	}
	/* looter
	 * Recupere les butins du personnage vaincu
	 * s'execute e la fin de chaque combat remporter
	 * PARAM :Personnage; le personnage vaincu
	 * Return :Arrayliste<butin> ; la liste des butins du personnage vaincu
	 */
	private ArrayList<Butin> looter(Personnage leDefunt){
		 
		 ArrayList<Butin> pasfait = new ArrayList<Butin>();
		return pasfait;
	}
	/*
	 * RecolteExperience
	 * 
	 * calcule l'experience gagnier durant le combat en fonction :
	 * De la difficulter du mob
	 * Du niveaux globale du mob
	 * s'execute e la fin de chaque combat remporter
	 * PARAM :Mob ; leMob vaincu
	 * Return la liste des butins du personnage vaincu
	 */
	private int recolteExperience(Mob leMob) {
		return -1;
	}
	
}
