package com.sogeti.algo17RPG;
import com.sogeti.algo17RPG.model.*;
import java.util.ArrayList;

import com.sogeti.algo17RPG.model.Arme;
import com.sogeti.algo17RPG.model.Armure;

public class Mob extends Personnage {
	private int difficulter;

	/**
	 * @param id
	 * @param nom
	 * @param pv
	 * @param pointAction
	 * @param niveau
	 * @param armePorter
	 * @param armurePorter
	 * @param leButin
	 * @param classe
	 * @param difficulter
	 */
	public Mob(int id, String nom, int pv, int pointAction, int niveau, Arme armePorter, Armure armurePorter,
			ArrayList<Butin> leButin, Classe classe, int difficulter) {
		super(id, nom, pv, pointAction, niveau, armePorter, armurePorter, leButin, classe);
		this.difficulter = difficulter;
	}
	/**
	 * @param id
	 * @param nom
	 * @param pv
	 * @param pointAction
	 * @param niveau
	 * @param armePorter
	 * @param armurePorter
	 * @param classe
	 * @param difficulter
	 */
	public Mob(int id, String nom, int pv, int pointAction, int niveau, Arme armePorter, Armure armurePorter
			, Classe classe, int difficulter) {
		super(id, nom, pv, pointAction, niveau, armePorter, armurePorter, classe);
		this.difficulter = difficulter;
	}

	public int getDifficulter() {
		return difficulter;
	}

	public void setDifficulter(int difficulter) {
		this.difficulter = difficulter;
	}
	

}
