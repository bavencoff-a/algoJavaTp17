package com.sogeti.algo17RPG;
import com.sogeti.algo17RPG.model.*;
import java.util.Random;
public class Templier implements Paladin {
	private int[] peutEquiper;
	private int bouclier;
	private Random rand = new Random();
	
	public Templier(int[] peutEquiper, int bouclier) {
		super();
		this.peutEquiper = peutEquiper;
		this.bouclier = bouclier;
	}
	public int[] getPeutEquiper() {
		return peutEquiper;
	}
	public void setPeutEquiper(int[] peutEquiper) {
		this.peutEquiper = peutEquiper;
	}
	public int getBouclier() {
		return bouclier;
	}
	public void setBouclier(int bouclier) {
		this.bouclier = bouclier;
	}
	@Override
	public int attaquer(Arme epee) {
		// TODO Auto-generated method stub
		return epee.getPointDeDegat();
	}
	/*
	 * porter
	 * 
	 * permet de savoir si le barbare peut porter l'armure mis en parametre
	 * 
	 * 
	 * */
	@Override
	public boolean porter(Armure mail) {
		boolean peutPorterCa = false;
		for (int i : peutEquiper) {
			if (mail.getTypeArmure().ordinal() == i) {
				peutPorterCa = true;
			}
		}
		
		return peutPorterCa;
	}
	/*
	 * prendEnMain
	 * 
	 * permet de savoir si le barbare peut porter l'arme mis en parametre
	 * 
	 * 
	 * */
	@Override
	public boolean prendEnMain(Arme epee) {
		boolean peutPorterCa = false;
		for (int i : peutEquiper) {
			if (epee.getTypeArme().ordinal() == i) {
				peutPorterCa = true;
			}
		}
		
		return peutPorterCa;
	}
	@Override
	public int soin() {
		// TODO Auto-generated method stub
		System.out.println("IN NOMINE !!");
		return rand.nextInt(6 - 1);
	}
	@Override
	public int defendre(Armure armure) {
		// TODO Auto-generated method stub
		return armure.getPointDeProtection() + this.bouclier;
	}
	
}
