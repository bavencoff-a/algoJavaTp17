package com.sogeti.algo17RPG;
import com.sogeti.algo17RPG.model.*;

public class Berserker implements Barbare {
	private int[] peutEquiper;
	private int rage;
	private int rageCourant;
	private Arme deuxiemArme = new Arme(TypeArme.Physique,1,2,"poing");
	public Berserker(int[] peutEquiper, int ragef) {
		super();
		this.peutEquiper = peutEquiper;
		
		rage = ragef;
		rageCourant = rage;
	}
	public int[] getPeutEquiper() {
		return peutEquiper;
	}
	public void setPeutEquiper(int[] peutEquiper) {
		this.peutEquiper = peutEquiper;
	}
	public int getRage() {
		return rage;
	}
	public void setRage(int rage) {
		rage = rage;
	}
	public void initRage() {
		rageCourant = rage;
	}
	/*
	 * attaquer
	 * 
	 * attaque avec son arme principale et rajoute le bonus de rage au degats ou non
	 * 
	 * 
	 * */
	@Override
	public int attaquer(Arme epee) {
		// TODO Auto-generated method stub
		int dega = 0;
		if(rageCourant > 0) {
			dega += 5;
			rageCourant --;
			System.out.println("RAGE !!");
		}
		dega += epee.getPointDeDegat() + attaqueDeuxiemMain(epee);
		return dega;
	}
	/*
	 * porter
	 * 
	 * permet de savoir si le barbare peut porter l'armure mis en parametre
	 * 
	 * 
	 * */
	public boolean porter(Armure mail) {
		boolean peutPorterCa = false;
		for (int i : peutEquiper) {
			if (mail.getTypeArmure().ordinal() == i) {
				peutPorterCa = true;
			}
		}
		
		return peutPorterCa;
	}
	/*
	 * prendEnMain
	 * 
	 * permet de savoir si le barbare peut porter l'arme mis en parametre
	 * 
	 * 
	 * */
	@Override
	public boolean prendEnMain(Arme epee) {
		boolean peutPorterCa = false;
		for (int i : peutEquiper) {
			if (epee.getTypeArme().ordinal() == i) {
				peutPorterCa = true;
			}
		}
		
		return peutPorterCa;
	}
	/*
	 * attaqueDeuxiemMain
	 * 
	 * attaque avec son arme secondaire et rajoute le bonus de rage au degat
	 * un malus de dega est apporter 
	 * 
	 * */
	@Override
	public int attaqueDeuxiemMain(Arme epeeDeux) {
		// TODO Auto-generated method stub
		return epeeDeux.getPointDeDegat()-1;
	}
	@Override
	public int defendre(Armure armure) {
		// TODO Auto-generated method stub
		
		return armure.getPointDeProtection();
	}
	@Override
	public Arme getDeuxiemArme() {
		// TODO Auto-generated method stub
		return this.deuxiemArme;
	}


	@Override
	public boolean setDeuxiemArme(Arme arme) {
		// TODO Auto-generated method stub
		boolean trouve = false;
		if (prendEnMain(arme)) {
			this.deuxiemArme = arme;
			trouve = true;
		}
		return trouve;
	}
	@Override
	public boolean equipeDeuxiemArme(Arme arme) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
}
