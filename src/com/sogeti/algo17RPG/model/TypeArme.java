/**
 * 
 */
package com.sogeti.algo17RPG.model;

/**
 * @author admin
 *
 */
public enum TypeArme {

	/**
	 * 
	 */
	Physique,
	Magique,
	Mixte
}
