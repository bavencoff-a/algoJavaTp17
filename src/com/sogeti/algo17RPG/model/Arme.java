/**
 * 
 */
package com.sogeti.algo17RPG.model;
/**
 * @author admin
 *
 */
public class Arme {
	private TypeArme typeArme;
	private int pointAction;
	private int pointDeDegat;
	private String nom;

	public TypeArme getTypeArme() {
		return typeArme;
	}

	public void setTypeArme(TypeArme typeArme) {
		this.typeArme = typeArme;
	}

	public int getPointAction() {
		return pointAction;
	}

	public void setPointAction(int pointAction) {
		this.pointAction = pointAction;
	}

	public int getPointDeDegat() {
		return pointDeDegat;
	}

	public void setPointDeDegat(int pointDeDegat) {
		this.pointDeDegat = pointDeDegat;
	}

	public Arme(TypeArme typeArme, int pointAction, int pointDeDegat, String nom) {
		super();
		this.typeArme = typeArme;
		this.pointAction = pointAction;
		this.pointDeDegat = pointDeDegat;
		this.nom = nom;
	}
	
	

}