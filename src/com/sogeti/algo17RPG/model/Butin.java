/**
 * 
 */
package com.sogeti.algo17RPG.model;

import java.util.ArrayList;

/**
 * @author admin
 *
 */
public class Butin {

	/**
	 * 
	 */
	private int fortune;
	private ArrayList<Arme> lesArmes;
	private ArrayList<Armure> lesArmures;
	/**
	 * @param fortune
	 * @param lesArmes
	 * @param lesArmures
	 */
	public Butin(int fortune, ArrayList<Arme> lesArmes, ArrayList<Armure> lesArmures) {
		super();
		this.fortune = fortune;
		this.lesArmes = lesArmes;
		this.lesArmures = lesArmures;
	}
	public int getFortune() {
		return fortune;
	}
	public void setFortune(int fortune) {
		this.fortune = fortune;
	}
	public ArrayList<Arme> getLesArmes() {
		return lesArmes;
	}
	public void setLesArmes(ArrayList<Arme> lesArmes) {
		this.lesArmes = lesArmes;
	}
	public boolean ajoutArme(Arme addArme) {
		return lesArmes.add(addArme);
	}
	public boolean ajoutArmure(Armure addArmure) {
		return lesArmures.add(addArmure);
	}
	public ArrayList<Armure> getLesArmures() {
		return lesArmures;
	}
	public void setLesArmures(ArrayList<Armure> lesArmures) {
		this.lesArmures = lesArmures;
	}
	


}
