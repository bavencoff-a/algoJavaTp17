/**
 * 
 */
package com.sogeti.algo17RPG.model;

/**
 * @author admin
 *
 */
public enum TypeArmure {

	/**
	 * 
	 */
	Physique,
	Magique,
	Mixte
}