/**
 * 
 */
package com.sogeti.algo17RPG.model;

/**
 * @author admin
 *
 */
public class Armure {
	private TypeArmure TypeArmure;
	private int pointDeProtection;
	private String nom;
	
	
	
	public TypeArmure getTypeArmure() {
		return TypeArmure;
	}

	public void setTypeArmure(TypeArmure TypeArmure) {
		this.TypeArmure = TypeArmure;
	}

	public int getPointDeProtection() {
		return pointDeProtection;
	}

	public void setPointDeProtection(int pointDeProtection) {
		this.pointDeProtection = pointDeProtection;
	}

	/**
	 * @param typeArmure
	 * @param pointDeProtection
	 * @param nom
	 */
	public Armure(TypeArmure typeArmure, int pointDeProtection, String nom) {
		super();
		TypeArmure = typeArmure;
		this.pointDeProtection = pointDeProtection;
		this.nom = nom;
		
	}


}

