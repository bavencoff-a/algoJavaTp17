/**
 * 
 */
package com.sogeti.algo17RPG.manager;
import com.sogeti.algo17RPG.model.*;
import com.sogeti.algo17RPG.*;

/**
 * @author admin
 *
 */
public class Combat {

	/**
	 * 
	 */
	private Personnage[] joueurs;
	
	private static final String DEGAT = "%s inflige %d de d�gats � %s, il reste %d vie � %s";
	private static final String VICTOIRE = "Le joueur %s gagne le match";
	private static final String NIVEAU = "Le joueur %s gagne 1 niveau, il est maintenant niveau %d !";
	
	
	//GESTION DES MESSAGES
	private void MessageVictoire(Personnage personnage) {
		System.out.println(String.format(VICTOIRE, personnage.getNom()));
	}
	
	
	public void MessageDegat(Personnage attaquant, Personnage defenseur) {
		System.out.println(String.format(DEGAT, attaquant.getNom(),
				attaqueSur(attaquant,defenseur), defenseur.getNom(),
				defenseur.getPv(), defenseur.getNom()));
	}
	
	public void MessageNiveau(Personnage personnage) {
		System.out.println(String.format(NIVEAU, personnage.getNom(), personnage.getNiveau()));
	}
	/*
	 * 
	 * 
	 * */
	public int attaqueSur(Personnage attaquant, Personnage defenseur) {
		Double pointDega = 0.0;
		int cumulArmure = 1;
		Double ratio = 1.00;
		System.out.println("test");
		
		switch (attaquant.getArmePorter().getTypeArme().ordinal()) {
		case 0:
			switch (defenseur.getArmurePorter().getTypeArmure().ordinal()) {
			case 0:
				ratio = 1.00;
				break;
			case 1:
				ratio =0.90;
				break;
			case 2:
				ratio =0.90;
				break;

			default:
				break;
			}
			
			break;
		case 1:
			switch (defenseur.getArmurePorter().getTypeArmure().ordinal()) {
			case 0:
				ratio =1.10;
				break;
			case 1:
				ratio =1.0;
				break;
			case 2:
				ratio =0.90;
				break;

			default:
				break;
			}
			break;
		case 2:
			switch (defenseur.getArmurePorter().getTypeArmure().ordinal()) {
			case 0:
				ratio =1.05;
				break;
			case 1:
				ratio =0.95;
				break;
			case 2:
				ratio =1.0;
				break;

			default:
				break;
			}
			break;

		default:
			break;
		}
		cumulArmure = (attaquant.getPointAction() / attaquant.getArmePorter().getPointAction()) * defenseur.defendre();


		pointDega = attaquant.attaquer() * ratio - cumulArmure;
		if(pointDega < 0) {
			pointDega = 0.0;
		}
		return pointDega.intValue();
	}

	//GESTION DU COMBAT
	public Combat() {
		int[] test = {1};
		
		this.joueurs = new Personnage[2];
		this.joueurs[0] = new Hero(
				0,//id
				"Jean",//nom
				20,//PVie
				5,//PAction
				2,//Niveau
				(new Arme(TypeArme.Physique,3,5,"glaive")),
				(new Armure(TypeArmure.Physique,3,"Pullover")),
				(new BarbareDeuxArmes(test)),
				3,//PDestin
				3//XP
				);

		this.joueurs[1] = new Mob(
				1,//id
				"Claude",//nom
				20,//PVie
				5,//PAction
				2,//Niveau
				(new Arme(TypeArme.Physique,3,5,"glaive" )),
				(new Armure(TypeArmure.Physique,3,"Pullover")),
				(new BarbareDeuxArmes(test)),
				3//Difficulte
				);
	}
	
	
	public void combatre() {
		boolean notEnded = true;
		int j = 0;
		while (notEnded) {
			for (int i = 0; notEnded && i < joueurs.length; i++) {
				
				if (joueurs[i].getPv() > 0) {
					Personnage defenseur = trouveDefenseur(i);
					
					if (defenseur == joueurs[i]) {
						MessageVictoire(joueurs[i]);
						notEnded = false;
					} else {
						//combat(joueurs[i], defenseur);

						defenseur.setPv(defenseur.getPv() - attaqueSur(joueurs[i], defenseur));

						MessageDegat(joueurs[i],defenseur);
					}
				
				}
			}
		}
	}
	
	private Personnage trouveDefenseur(int attaquant) {
		int i = attaquant;
		Personnage defenseur = null;

		// Boucle tant que l'on est pas revenu au départ
		boolean flag = true;
		do {
			// Vérification de l'indice pour ne pas sortir du tableau
			if (i + 1 == joueurs.length) {
				i = 0;
			} else {
				i++;
			}

			if (joueurs[i].getPv() > 0) {
				defenseur = joueurs[i];
				flag = false;
			}
			
			
		} while (flag && i != attaquant);
		
		// Retourne le defenseur suivant ou null si la partie est fini
		return defenseur;
	}

	public void combat(Personnage attaquant, Personnage defenseur) {
		/*int pa = attaquant.getPointAction();
		// L'attaquant s'équipe de l'arme.
		pa--;
		while (pa - attaquant.getArmePorter().getPointAction() > 0) {
			pa -= attaquant.getArmePorter().getPointAction();
			if (attaquant.getArmePorter().getPointDeDegat() - defenseur.getArmurePorter().getPointDeProtection() > 0) {
				defenseur.setPv(
						defenseur.getPv() - (attaquant.getArmePorter().getPointDeDegat() - defenseur.getArmurePorter().getPointDeProtection()));
				MessageDegat(attaquant, defenseur);
				if (defenseur.getPv() <= 0) {
					return;
				}
			} else {
				MessageDegat(attaquant,defenseur);
			}
		}*/
		
	}
	
	
	
}
